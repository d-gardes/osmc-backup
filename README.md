# OSMC Backup

> Early alpha stage. May lead to data loss ! Use carefully

OSMC automatic backup system based on BorgBackup
> Tested with BorgBackup 1.2.x

## Configuration

All the configuration is available in ``config.sh`` to automate backups and common actions.

## ``backup.sh``

Script used to automate backups of OSMC.
> Script non interactive. Can be automated with cron (see install.sh)

## ``restore.sh``

## ``install.sh``

## ``uninstall.sh``
#!/usr/bin/bash

SCRIPT_DIR=$(cd $(dirname "${BASH_SOURCE[0]}") && pwd)
source $SCRIPT_DIR/config.sh

if [ $# -ne 1 ] ; then
	echo "Usage: backup.sh BACKUP_NAME"
	exit
fi

cd ~

# Step 1: Convert backup name
# Replace '/' by '-'
BACKUP_NAME="${1//\//\-}"

# Step 2: Backup
$SCRIPT_DIR/borg create $BORG_ARGUMENTS_BACKUP ::$BACKUP_NAME .

# Step 3: Prune
$SCRIPT_DIR/borg prune $BORG_ARGUMENTS_PRUNE ::

# Step 4: Compact
$SCRIPT_DIR/borg -p compact ::
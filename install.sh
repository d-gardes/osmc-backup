#!/usr/bin/bash

SCRIPT_DIR=$(cd $(dirname "${BASH_SOURCE[0]}") && pwd)

source $SCRIPT_DIR/config.sh

# Step 1: Install BorgBackup

# wget -O borg https://github.com/borgbackup/borg/releases/latest/download/borg-linux64
# Vero is running an ARM SOC !
wget -O borg https://borg.bauerj.eu/bin/borg-1.2.6-armv6
# TODO Find a way to get the latest version like on GitHub
chmod u+x $SCRIPT_DIR/borg
BORG_BACKUP_VERSION=`$SCRIPT_DIR/borg --version`
if [ ${BORG_BACKUP_VERSION:5:4} != "1.2." ] ; then
	echo "!! WARNING !! This script may be incompatible with this version of BorgBackup. Please use 1.2.X
Downloaded: $BORG_BACKUP_VERSION"
fi

# Step 2: Setup SSH connexion
if [ ${BORG_REPO::4} = "ssh:" ] ; then

	# TODO Makes the "No such file or directory" error silent
	$(ls ~/.ssh/*.pub)
	if [ $? -eq 2 ]; then
    	echo "SSH key not found. Creating ..."
		mkdir ~/.ssh
		ssh-keygen -t ed25519
	fi

	echo "Copying SSH key to remote server"
	ssh-copy-id ${BORG_REPO:6}
	# TODO Fix the broken ssh-copy-id
fi

# Step 3: Creation of the BorgBackup repo
$SCRIPT_DIR/borg init --make-parent-dirs -e $ENCRYPTION_MODE ::

# Step 4: Add automatic backups to cron
if [ $CRON_ENABLED != "false" ] ; then
	sudo apt install cron
	if [ $CRON_DAILY != "false" ] ; then
		echo "$CRON_MINUTE $CRON_HOUR * * * osmc /usr/bin/bash $SCRIPT_DIR/backup.sh Daily-\$(date +"\\%b-\\%d-\\%Y_\\%Hh\\%M")" >> ./BorgBackup
	else
		if [ $CRON_WEEKLY != "false" ] ; then
			echo "$CRON_MINUTE $CRON_HOUR * * 1 osmc /usr/bin/bash $SCRIPT_DIR/backup.sh Weekly-\$(date +"\\%b-\\%d-\\%Y_\\%Hh\\%M")" >> ./BorgBackup
		else
			echo "$CRON_MINUTE $CRON_HOUR 1 * * osmc /usr/bin/bash $SCRIPT_DIR/backup.sh Monthly-\$(date +"\\%b-\\%d-\\%Y_\\%Hh\\%M")" >> ./BorgBackup
		fi
	fi
	sudo mv ./BorgBackup /etc/cron.d/BorgBackup
	sudo chown root:root /etc/cron.d/BorgBackup
	sudo chmod 744 /etc/cron.d/BorgBackup
fi

# Step 5: Ask for initial backup
echo "Would you like to create an initial backup (Recommended) ? [y/n]"
read INITIAL
if [ $INITIAL = "y" ] ; then
	$SCRIPT_DIR/backup.sh Initial-$(date +"%b-%d-%Y_%Hh%M")
fi
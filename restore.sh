#!/usr/bin/bash

SCRIPT_DIR=$(cd $(dirname "${BASH_SOURCE[0]}") && pwd)
source $SCRIPT_DIR/config.sh

function unmount_backup {
	$SCRIPT_DIR/borg umount $MOUNT_PATH
	sudo rmdir $MOUNT_PATH
	echo "Starting Kodi ..."
	sudo systemctl start mediacenter
	echo "Done !"
}

if [ $1 -e "umount" ] ; then
	unmount_backup
	exit
fi

echo "OSMC Restore

[1] Complete restore. (Restore a backup and overwrite all OSMC files)
[2] Partial / manual restore. (Mount a backup, user can copy files needed without any overwrite)
[3] End partial / manual restore. (Unmount backup when manual restore finished by user)

What do you want to do ?"

read USER_CHOICE

if [ $USER_CHOICE = 1 ] ; then
	$SCRIPT_DIR/borg list ::
	echo "Please type the name of the archive to extract from the list :"
	read ARCHIVE_NAME
	echo "Stopping Kodi ..."
	sudo systemctl stop mediacenter
	echo "Restoring $ARCHIVE_NAME ..."
	cd ~
	$SCRIPT_DIR/borg extract $BORG_ARGUMENTS_EXTRACT ::$ARCHIVE_NAME
	echo "Starting Kodi ..."
	sudo systemctl start mediacenter
	echo "Done !"
fi

if [ $USER_CHOICE = 2 ] ; then
	echo "Stopping Kodi ..."
	sudo systemctl stop mediacenter
	sudo mkdir $MOUNT_PATH
	sudo chown `id -u`:`id -g` $MOUNT_PATH
	$SCRIPT_DIR/borg mount $BORG_ARGUMENTS_MOUNT :: $MOUNT_PATH
	echo "Backups repository mounted at '$MOUNT_PATH'.
Individual backups are lazily loaded expect some delay when navigating to an archive for the first time.

When manual restore finished you can unmount the backups with './restore.sh umount' or with 3rd option in the menu"
fi

if [ $USER_CHOICE = 3 ] ; then
	unmount_backup
fi
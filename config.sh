#!/usr/bin/bash

#######################################################################################
#               DO NOT EDIT THIS SECTION PLEASE

# If config.sh permission != 600
SCRIPT_DIR=$(cd $(dirname "${BASH_SOURCE[0]}") && pwd)
if [ `stat -c "%a" $SCRIPT_DIR/config.sh` != "600" ] ; then
	chmod 600 $SCRIPT_DIR/config.sh
fi
#######################################################################################
# 				BEGIN OF CONFIGURATION FILE

# Change me to 'true' when configuration is done to enable other scripts.
IS_CONFIGURED=false

# BorgBackup's repository encryption mode.
# See https://borgbackup.readthedocs.io/en/stable/usage/init.html#encryption-mode-tldr
ENCRYPTION_MODE=repokey

# Path used to mount backups repository for manual restores
MOUNT_PATH="/mnt/BorgRestore"

# Enable (or not) automatic backups with cron
CRON_ENABLED=false
# Time of the start the backups (default at 5 am)
CRON_HOUR=5
CRON_MINUTE=0
# Frequency of the automatic backups
# If Dayly and weekly are disabled the backups will be monthly
CRON_DAILY=false
CRON_WEEKLY=true

# Specify custom arguments for BorgBackup commands.
# See :
# https://borgbackup.readthedocs.io/en/stable/usage/create.html
# Default value: BORG_ARGUMENTS_BACKUP='--progress --stats -C zlib  --exclude ".cache/" --exclude "osmc-backup" --exclude ".kodi/temp/"' 
BORG_ARGUMENTS_BACKUP='--progress --stats -C zlib  --exclude ".cache/" --exclude "osmc-backup" --exclude ".kodi/temp/"'
# https://borgbackup.readthedocs.io/en/stable/usage/prune.html
# Default value: BORG_ARGUMENTS_PRUNE='--progress --stats --keep-daily=7 --keep-weekly=4 --keep-monthly=1'
BORG_ARGUMENTS_PRUNE='--progress --stats --keep-daily=7 --keep-weekly=4 --keep-monthly=1'
# https://borgbackup.readthedocs.io/en/stable/usage/extract.html
# Default value: BORG_ARGUMENTS_EXTRACT='--progress'
BORG_ARGUMENTS_EXTRACT='--progress'
# https://borgbackup.readthedocs.io/en/stable/usage/mount.html
# Default value: BORG_ARGUMENTS_MOUNT='--progress -o ro'
BORG_ARGUMENTS_MOUNT='--progress -o ro'


#########################################""

# BorgBackup configuration. See documentation for further informations
# https://borgbackup.readthedocs.io/en/stable/usage/general.html#environment-variables
# All BorgBackup environement variables can be set if you need it

# See https://borgbackup.readthedocs.io/en/stable/usage/general.html#repository-urls
export BORG_REPO='ssh://user@host:port/path/to/repo'

#export BORG_PASSPHRASE=''
#export BORG_PASSCOMMAND=''


#######################################################################################
#               DO NOT EDIT BELOW THIS LINE PLEASE
if [ $IS_CONFIGURED = "false" ] ; then
	echo "Please edit 'config.sh' before running any script.
Exiting ..."
	exit
fi
#!/usr/bin/bash

SCRIPT_DIR=$(cd $(dirname "${BASH_SOURCE[0]}") && pwd)

source $SCRIPT_DIR/config.sh

# Step 1: Delete BorgBackup repo ?

echo "Would you like to delete the remote repo ? [y/n]
!! WARNING !! THIS WILL PERMANENTLY DESTROY ALL YOUR BACKUPS !!"
read DELETE
if [ $DELETE = "y" ] ; then
	borg delete ::
fi

# Step 2: Delete cron automatic backups
sudo rm /etc/cron.d/BorgBackup

# Step 3: Self-destruct

if [ -z ${BORG_CACHE_DIR+x} ]; then
	if [ -z ${BORG_BASE_DIR+x} ]; then
		rm -r ~/.cache/borg
	else
		rm -r $BORG_BASE_DIR/.cache/borg
	fi
else
	rm -r $BORG_CACHE_DIR/borg
fi




